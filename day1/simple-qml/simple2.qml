import QtQuick 2.15

Rectangle {
    width: 400
    height: 400
    color: "green"

    Rectangle {
        id: red
        width: 200
        height: 200
        anchors { right: parent.right; bottom: parent.bottom }
        color: "red"
    }

    Rectangle {
        id: yellow
        width: 100
        height: 100
        color: "yellow"
        anchors { left: parent.left; top: parent.top }
        Behavior on rotation { NumberAnimation { duration: 1000 } }
    }

    states: State {
        name: "moved"
        AnchorChanges { target: yellow; anchors { left: red.left; top: red.top } }
        PropertyChanges { target: yellow; color: "brown"; rotation: 90; scale: 0.5; opacity: 0.5 }
    }

    transitions: Transition {
        AnchorAnimation { duration: 1000; easing.type: Easing.OutBounce }
        ColorAnimation { duration: 1000 }
        //NumberAnimation { property: "rotation"; duration: 1000 }
        NumberAnimation { properties: "scale, opacity"; duration: 1000 }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: parent.state = (parent.state === "moved") ? "":"moved"
    }
}
