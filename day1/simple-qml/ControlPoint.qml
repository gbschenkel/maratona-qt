import QtQuick 2.15

Rectangle {
    id: control
    width: 20; height: width
    color: "red"; radius: width/2

    MouseArea {
        anchors.fill: parent
        drag {
            target: parent
            minimumX: 0; maximumX: control.parent.width-20
            minimumY: 0; maximumY: control.parent.height-20
        }
    }
}
