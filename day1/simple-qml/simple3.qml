import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Shapes 1.15

Rectangle {
    id: rootItem
    width: 400
    height: 400
    color: "black"

    Image {
        id: spaceShip
        x: (parent.width-width)/2; y: (parent.height-height)/2
        z: 1
        source: "spaceship.png"
    }

    ControlPoint { id: control1; x: parent.width-20 }
    ControlPoint { id: control2; x: parent.width-20; y: parent.height-20 }
    ControlPoint { id: control3 }
    ControlPoint { id: control4; y: parent.height-20 }

    Shape {
        anchors.fill: parent
        ShapePath {
            id: path
            fillColor: "transparent"
            startX: parent.width/2; startY: parent.height/2
            PathCubic {
                x: parent.width/2; y: parent.height/2
                control1X: control1.x; control1Y: control1.y
                control2X: control2.x; control2Y: control2.y
            }
            PathCubic {
                x: parent.width/2; y: parent.height/2
                control1X: control3.x; control1Y: control3.y
                control2X: control4.x; control2Y: control4.y
            }
        }
    }

    PathAnimation {
        id: pathAnimation
        target: spaceShip
        anchorPoint: Qt.point(spaceShip.width/2, spaceShip.height/2)
        orientation: PathAnimation.TopFirst
        endRotation: 0
        path: Path {
            startX: parent.width/2; startY: parent.height/2
            pathElements: path.pathElements
        }
        loops: Animation.Infinite
        //easing.type: Easing.InBounce
        running: true
        duration: 3000
    }
}
